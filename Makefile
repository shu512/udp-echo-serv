CCFLAGS = -Wall -Wextra -pedantic -O2
LDFLAGS =

CLIENT_SRC = client.c
SERVER_SRC = server.c

CLIENT_PROG = client
SERVER_PROG = server

CC = gcc

all: $(CLIENT_PROG) $(SERVER_PROG)

$(CLIENT_PROG): $(CLIENT_SRC)
	$(CC) $^ $(LDFLAGS) $(CCFLAGS) -o $@

$(SERVER_PROG): $(SERVER_SRC)
	$(CC) $^ $(LDFLAGS) $(CCFLAGS) -o $@

clean:
	rm -f $(CLIENT_PROG) $(SERVER_PROG) *.o

