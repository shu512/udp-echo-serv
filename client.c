#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#include <sys/socket.h>

#include <arpa/inet.h>

#define BUF_SIZE		1024
#define ADDR_DEFAULT	"127.0.0.1"
#define PORT_DEFAULT	8888

int main(int argc, char *argv[])
{
	int sd;
	struct sockaddr_in saddr;
	socklen_t saddr_len;
	char *ip = ADDR_DEFAULT;
	char buf[BUF_SIZE] = "a capillo usque ad ungues";
	int buf_size;
	int port = 0;


	if ( (sd = socket(AF_INET , SOCK_DGRAM, IPPROTO_UDP)) < 0 )
	{
		perror("socket()");
		exit(EXIT_FAILURE);
	}

	if ( argc > 1 )
		ip = argv[1];

	if ( argc > 2 )
		port = atoi(argv[2]);
	if ( port < 1024 )
		port = PORT_DEFAULT;

	memset(&saddr, 0, sizeof(saddr));
	saddr.sin_family = AF_INET;
	saddr.sin_port = htons(port);
	saddr.sin_addr.s_addr = inet_addr(ip);
	saddr_len = sizeof(saddr);

	if ( argc > 3 )
		strncpy(buf, argv[3], BUF_SIZE);
	buf[BUF_SIZE - 1] = '\0';
	buf_size = strlen(buf) + 1;

	printf("[client] Send to server (%d bytes): %s\n", buf_size, buf);
	if ( sendto(sd, buf, buf_size, 0, (struct sockaddr *)&saddr, saddr_len) < 0 )
	{
		perror("sendto()");
		exit(EXIT_FAILURE);
	}

	if ( (buf_size = recvfrom(sd, buf, BUF_SIZE, 0, (struct sockaddr *)&saddr, &saddr_len)) < 0)
	{
		perror("recvfrom()");
		exit(EXIT_FAILURE);
	}
	printf("[client] Received from server (%d bytes): %s\n", buf_size, buf);

	close(sd);

	return 0;
}
