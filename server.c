#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/socket.h>

#include <netinet/in.h>

#include <arpa/inet.h>

#define BUF_SIZE		1024
#define PORT_DEFAULT	8888

int main(int argc, char *argv[])
{
	int sd;
	struct sockaddr_in saddr;
	struct sockaddr_in caddr;
	socklen_t caddr_len = sizeof(caddr);
	char buf[BUF_SIZE];
	int buf_read;
	int port = 0;

	if ( (sd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0 )
	{
		perror("socket()");
		exit(EXIT_FAILURE);
	}

	if ( argc > 1 )
		port = atoi(argv[1]);
	if ( port < 1024 )
		port = PORT_DEFAULT;

	memset(&saddr, 0, sizeof(saddr));
	saddr.sin_family = AF_INET;
	saddr.sin_port = htons(port);
	saddr.sin_addr.s_addr = htonl(INADDR_ANY);

	if ( bind(sd, (struct sockaddr *)&saddr, sizeof(saddr)) < 0 )
	{
		perror("bind()");
		exit(EXIT_FAILURE);
	}

	while ( 1 )
	{
		printf("[server] Waiting for cliesnt's data\n");

		buf_read = recvfrom(sd, buf, BUF_SIZE, 0, (struct sockaddr *)&caddr, &caddr_len);
		printf("[server] Received from client (%d bytes): %s\n", buf_read, buf);

		if ( buf_read < 0 ) 
		{
			perror("recvfrom()");
			exit(EXIT_FAILURE);
		}
		else if ( buf_read > 0 )
		{
			buf_read--;
			for ( int i = 0; i < buf_read / 2; i++ )
			{
				char t = buf[i];
				buf[i] = buf[buf_read - i - 1];
				buf[buf_read - i - 1] = t;
			}

			printf("[server] Send to client (%d bytes): %s\n", buf_read + 1, buf);
			sendto(sd, buf, buf_read + 1, 0, (struct sockaddr *)&caddr, caddr_len);
		}
	}

	return 0;
}
